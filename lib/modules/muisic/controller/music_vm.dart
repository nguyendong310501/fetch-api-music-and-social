import 'dart:async';

import 'package:clone_app_social/modules/muisic/controller/call_api.dart';
import 'package:clone_app_social/modules/muisic/model/repo.dart';
import 'package:clone_app_social/providers/bloc_provider.dart';
import 'package:rxdart/rxdart.dart';

class ListMusicsRxDartBloc extends BlocBase {
  final _postsCtrl = BehaviorSubject<List<MusicModel>?>();
  Stream<List<MusicModel>?> get postsStream => _postsCtrl.stream;
  List<MusicModel>? get postsValue => _postsCtrl.stream.value;

  Future<void> getMusic() async {
    try {
      final res = await ListMusicRepo().fetchMusic();
      if (res != null) {
        _postsCtrl.sink.add(res.music);
      }
    } catch (e) {
      _postsCtrl.sink.addError('Cannot fetch list posts right now!!!');
    }
  }

  @override
  void dispose() {
    _postsCtrl.close();
  }
}
