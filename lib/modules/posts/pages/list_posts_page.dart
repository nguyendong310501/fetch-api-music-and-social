import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:clone_app_social/modules/posts/blocs/list_posts_rxdart_bloc.dart';
import 'package:clone_app_social/modules/posts/models/post.dart';
import 'package:clone_app_social/modules/posts/widgets/post_item_remake.dart';

class ListPostsPage extends StatefulWidget {
  const ListPostsPage({Key? key}) : super(key: key);

  @override
  _ListPostsPageState createState() => _ListPostsPageState();
}

class _ListPostsPageState extends State<ListPostsPage> {
  final _postsBloc = ListPostsRxDartBloc();

  @override
  void initState() {
    super.initState();
    _postsBloc.getPosts();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Stack(
      children: [
        CustomScrollView(
          physics: const AlwaysScrollableScrollPhysics(),
          slivers: <Widget>[
            const SliverAppBar(
              title: Text(
                'Home Music me',
                style: TextStyle(color: Colors.black),
              ),
              snap: true,
              floating: true,
              elevation: 1,
              forceElevated: true,
              backgroundColor: Colors.white,
            ),
            CupertinoSliverRefreshControl(
              onRefresh: _postsBloc.getPosts,
            ),
            StreamBuilder<List<Post>?>(
                stream: _postsBloc.postsStream,
                builder: (context, snapshot) {
                  if (snapshot.data == null) {
                    return const SliverFillRemaining(
                      child: Center(child: CircularProgressIndicator()),
                    );
                  }

                  if (snapshot.hasError) {
                    return const SliverFillRemaining(
                        child: Center(
                      child: Text('Something went wrong'),
                    ));
                  }

                  return SliverList(
                    delegate: SliverChildBuilderDelegate(
                      (context, index) {
                        final post = snapshot.data![index];
                        return PostItem(post: post);
                      },
                      childCount: snapshot.data?.length ?? 0,
                    ),
                  );
                }),
            const SliverPadding(padding: EdgeInsets.only(bottom: 120)),
          ],
        ),
        Positioned(
            right: 15,
            bottom: 15,
            child: Container(
              decoration: BoxDecoration(
                  color: Colors.blue,
                  borderRadius: BorderRadius.circular(100.0)),
              padding: EdgeInsets.all(10),
              child: IconButton(
                icon: Icon(
                  Icons.home,
                  color: Colors.white,
                ),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
            ))
      ],
    ));
  }
}
